//
//  EditorViewController.swift
//  JJ_Char_Detector
//
//  Created by Japahar Jose on 24/08/19.
//  Copyright © 2019 *compasquare*. All rights reserved.
//

import UIKit

class EditorViewController: UIViewController {
    var textView : WPTextView!

    @IBOutlet weak var handWrirtngView: UIView!
    @IBOutlet weak var displayTextLbl: UILabel!
    @IBOutlet weak var typeSegment: UISegmentedControl!
    @IBOutlet weak var txtStatusLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        setInputView()
        // Do any additional setup after loading the view.
    }
    func setInputView(){
        txtStatusLbl.text = ""
        self.textView = WPTextView.createTextView(self.view.bounds)
        // self.textView.delegate = self
        self.handWrirtngView.addSubview(self.textView)
        self.textView.translatesAutoresizingMaskIntoConstraints = false
        self.textView.setInputMethod(InputSystem_WriteAnywhere)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.showSpinningWheel(notification:)), name: NSNotification.Name(rawValue: "writtenLetters"), object: nil)

    
    }
   @objc  func showSpinningWheel(notification: NSNotification) {
        if let text = notification.userInfo?["Text"] as? String {
            switch typeSegment.selectedSegmentIndex{
            case 0  :
                txtStatusLbl.text = text.isUppercase() ? "Valid Input" : "Invallid Input"
                txtStatusLbl.textColor = text.isUppercase() ? UIColor.green : UIColor.red

                break
            case 1  :
                txtStatusLbl.text = text.isLowercase() ? "Valid Input" : "Invallid Input"
                txtStatusLbl.textColor = text.isLowercase() ? UIColor.green : UIColor.red
                break
            case 2  :
                    txtStatusLbl.text = text.isNumber ? "Valid Input" : "Invallid Input"
                    txtStatusLbl.textColor = text.isNumber ? UIColor.green : UIColor.red

                break
            default:
                break
            }
            displayTextLbl.text = text
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension String  {
    var isNumber: Bool {
        return !isEmpty && rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
    }
     func isLowercase() -> Bool {
        let set = CharacterSet.lowercaseLetters
        for character in self {
            if let scala = UnicodeScalar(String(character)) {
                if !set.contains(scala) {
                    return false
                }
            }
        }
        return true
    }
    
     func isUppercase() -> Bool {
        let set = CharacterSet.uppercaseLetters
        for character in self {
            if let scala = UnicodeScalar(String(character)) {
                if !set.contains(scala) {
                    return false
                }
            }
        }
        return true
    }
}
