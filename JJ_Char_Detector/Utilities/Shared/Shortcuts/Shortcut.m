

#import "Shortcut.h"
#import "OptionKeys.h"
#import "LanguageManager.h"

@implementation Shortcut

@synthesize		command;
@synthesize		name, text;
@synthesize		offset, enabled, addToMenu, comment, showInPanel;

- (id)initWithName:(NSString *)cmdName shortcut:(WPSystemShortcut)cmd
{
    self = [super init];
    if (self)
    {
        self.command = cmd;
        self.name = cmdName;
        self.text = nil;
        self.enabled = YES;
        self.showInPanel = NO;
        self.addToMenu = NO;
        self.offset = 0;
    }
    return self;
}

- (NSString *)text
{
    NSDate * date = [NSDate date];
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
    NSString * customFormat = [[NSUserDefaults standardUserDefaults] objectForKey:kTextEditCustomDateFomratString];
    NSInteger currentformat = [[NSUserDefaults standardUserDefaults] integerForKey:kTextEditDateFomratID];
    if ( currentformat < NSDateFormatterShortStyle )
        currentformat = NSDateFormatterMediumStyle;
    
    NSString * languageCode = [LanguageManager sharedManager].languageCode;
    NSLocale * loc = [NSLocale localeWithLocaleIdentifier:languageCode];
    if ( loc != nil )
        [dateFormatter setLocale:loc];
    
    // process current date
    switch( self.command )
    {
        case kWPSysShortcutDate :
            // get current date
            if ( currentformat > NSDateFormatterFullStyle && customFormat != nil )
            {
                [dateFormatter setDateFormat:customFormat];
            }
            else
            {
                [dateFormatter setDateStyle:currentformat];
                [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
            }
            self.text = [NSString stringWithFormat:@"%@ ", [dateFormatter stringFromDate:date]];
            break;
            
        case kWPSysShortcutTime :
            // get current time
            [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
            [dateFormatter setDateStyle:NSDateFormatterNoStyle];
            
            self.text = [NSString stringWithFormat:@"%@ ", [dateFormatter stringFromDate:date]];
            break;
            
        case kWPSysShortcutDateTime :
            [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
            [dateFormatter setDateStyle:(currentformat > NSDateFormatterFullStyle) ? NSDateFormatterMediumStyle : currentformat];
            self.text = [NSString stringWithFormat:@"%@ ", [dateFormatter stringFromDate:date]];
            break;
            
        default:
            break;
    }
#if !__has_feature(objc_arc)
    [dateFormatter release];
#endif
    return text;
}

- (NSString *) shortcutToCsvString
{
    NSMutableString *	str = [[NSMutableString alloc] init];
    
    // name, text, enabled, offset
    
    [str appendFormat:@"\"%@\",", self.name];
    [str appendString:[self textToString:self.text returns:@"\n"]];
    [str appendString:@","];
    [str appendFormat:@"\"%@\",\"%ld\",", self.enabled ? @"YES" : @"NO", (long)self.offset];
    [str appendFormat:@"\"%@\"\n", self.addToMenu ? @"YES" : @"NO"];
#if !__has_feature(objc_arc)
    return [str autorelease];
#else
    return str;
#endif
}

- (void) insertShortcut:(id)sender
{
    NSLog( @"%@", self.text );
    
}

- (NSString *)textToString:(NSString *)strText returns:(NSString *)ret
{
    if ( strText == nil || [strText length] < 1 )
        return @"";
    NSMutableString *	str = [[NSMutableString alloc] initWithString:@"\""];
    
    for ( NSUInteger i = 0; i < [strText length]; i++ )
    {
        unichar chr = [strText characterAtIndex:i];
        if ( chr == '\"' )
        {
            [str appendString:@"\""];
        }
        else if ( chr == '\r' )
            continue;
        else if ( chr == '\n' )
        {
            [str appendString:ret];
            continue;
        }
        [str appendString:[NSString stringWithCharacters:&chr length:1]];
    }
    [str appendString:@"\""];
#if !__has_feature(objc_arc)
    return [str autorelease];
#else
    return str;
#endif //
}

#if !__has_feature(objc_arc)
-(void)dealloc
{
    [name release];
    [text release];
    [super dealloc];
}
#endif //


@end
