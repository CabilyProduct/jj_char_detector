

#import <Foundation/Foundation.h>

typedef enum {
    kWPSysShortcutUnknown = -1,
    kWPSysShortcutCom,
    kWPSysShortcutFtp,
    kWPSysShortcutNet,
    kWPSysShortcutDate,
    kWPSysShortcutDateTime,
    kWPSysShortcutOrg,
    kWPSysShortcutWww,
    kWPSysShortcutTime,
    kWPSysShortcutSelectAll,
    kWPSysShortcutCopy,
    kWPSysShortcutCut,
    kWPSysShortcutPaste,
    kWPSysShortcutRedo,
    kWPSysShortcutSupport,		
    kWPSysShortcutUndo,			
    
    kWPSysShortcutTotal
} WPSystemShortcut;

#define MENU_MAX_SHORTCUTS	6


@interface Shortcut : NSObject
{
    WPSystemShortcut	command;
    NSString *			name;
    NSString *			text;
    NSString *          comment;
    Boolean				enabled;
    Boolean				addToMenu;
    NSInteger			offset;
}

@property(nonatomic) Boolean			enabled;
@property(nonatomic) NSInteger			offset;
@property(nonatomic) Boolean			addToMenu;
@property(nonatomic) WPSystemShortcut	command;
@property(nonatomic, copy) NSString	*	text;
@property(nonatomic, copy) NSString	*	name;
@property(nonatomic, copy) NSString	*	comment;
@property(nonatomic) Boolean			showInPanel;

- (id) initWithName:(NSString *)cmdName shortcut:(WPSystemShortcut)cmd;
- (NSString *) shortcutToCsvString;
- (NSString *) textToString:(NSString *)text returns:(NSString *)ret;
- (void) insertShortcut:(id)sender;

@end
