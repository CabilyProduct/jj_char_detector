

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "RecognizerWrapper.h"
#import "Shortcut.h"

@protocol ShortcutsDelegate;
@protocol ShortcutsDelegateUI;

#define PANEL_COMMANDS      8

@interface Shortcuts : NSObject
{
	//id<ShortcutsDelegate>		delegate;
	//id<ShortcutsDelegateUI>	delegateUI;
	Boolean						modified;
	
@private
	NSMutableArray	*	_shortcutsSys;
	NSMutableArray	*	_shortcutsUser;
	RECOGNIZER_PTR		_recognizer;
	NSString *			_userFileName;
}

@property(assign) id<ShortcutsDelegate>		delegate;
@property(assign) id<ShortcutsDelegateUI>	delegateUI;
@property(nonatomic) Boolean				modified;

+ (Shortcuts *) sharedShortcuts;

- (BOOL) enableRecognizer:(BOOL)bEnableReco;
- (BOOL) recognizeInkData:(INK_DATA_PTR)inkData;
- (BOOL) isEnabled;
- (BOOL) resetRecognizer;
- (Shortcut *) findByName:(NSString *)name;
- (void) addUserShortcut:(Shortcut *)sc;
- (void) deleteUserShortcut:(Shortcut *)sc;
- (BOOL) saveUserShortcuts;
- (BOOL) loadUserShortcuts;
- (void) newShortcut;
- (NSInteger) countUser;
- (NSInteger) countSystem;
- (Shortcut *) userShortcutByIndex:(NSInteger)index;
- (Shortcut *) sysShortcutByIndex:(NSInteger)index;
- (BOOL) process:(Shortcut *)sc;
- (void) saveSystemShortcuts;
- (BOOL) reloadUserShorcuts;

@end

@protocol ShortcutsDelegate<NSObject>
@required

- (BOOL) ShortcutsRecognizedShortcut:(Shortcut*)sc withGesture:(GESTURE_TYPE)gesture  offset:(NSInteger)offset;
- (NSString *) ShortcutGetSelectedText:(Shortcut*)sc withGesture:(GESTURE_TYPE)gesture  offset:(NSInteger)offset;

@end

@protocol ShortcutsDelegateUI<NSObject>
@optional

- (void) ShortcutsUIEditShortcut:(Shortcuts *)shortcuts shortcut:(Shortcut*)sc isNew:(BOOL)addNew;

@end

