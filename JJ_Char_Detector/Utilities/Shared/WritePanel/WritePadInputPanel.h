

#import <UIKit/UIKit.h>
#import "RecognizerWrapper.h"
#import "Shortcuts.h"

#define MAX_QUEUE_SIZE          512


@class WritePadInputView;
@class WritePadInputPanel;

/////////////////////////////////////////////////////////////////////////////

@interface InkObject : NSObject
{
	INK_DATA_PTR		inkData;
}

@property(nonatomic, assign) INK_DATA_PTR		inkData;

- (id)initWithInkData:(INK_DATA_PTR)initalData;
- (void) sortInk;

@end


@interface WPCurrentStrokeView : UIView
{
@private
    CGPoint                     _hoverPoint;
}

@property(nonatomic, weak) WritePadInputPanel *		panel;

- (void) setHoverLocation:(CGPoint)point;

@end


@interface WPInkView : UIView
{
}

@property(nonatomic, weak) WritePadInputPanel *		panel;

+ (void) _renderLine:(CGStroke)points pointCount:(int)count withWidth:(float)width withColor:(UIColor *)color;
+ (void) _renderLine:(CGStroke)points pointCount:(int)count inContext:(CGContextRef)context withWidth:(float)width withColor:(UIColor *)color;

@end

/////////////////////////////////////////////////////////////////////////////

@protocol WritePadInputPanelDelegate;

@interface WritePadInputPanel : UIView
{
    Shortcuts  *        _shortcuts;
	CGStroke			ptStroke;
	int					strokeLen;
	int					strokeMemLen;	
	Boolean				_firstTouch;
	CGPoint				_previousLocation;
	NSTimer *			_timerTouchAndHold;
	INK_DATA_PTR		inkData;
	GESTURE_TYPE		gesturesEnabledIfEmpty;
	GESTURE_TYPE		gesturesEnabledIfData;
	float				strokeWidth;
		
	CGPoint				_inkQueue[MAX_QUEUE_SIZE];
	int					_inkQueueGet, _inkQueuePut;
	
	NSCondition		*	_inkQueueCondition;
	Boolean				_runInkThread;
	Boolean				_bAsyncRecoEnabled;
	Boolean				_bAsyncInkCollector;
	NSLock *			_recoLock;
	NSCondition		*	_recoCondition;
	NSLock *			_inkLock;
	
	Boolean				_bSelectionMode;
	Boolean				_bAddStroke;
	Boolean				_bSendTouchToEdit;
	Boolean				_bShowingMenu;
	
	Boolean				_movingMarker;
    Boolean             _bStylusOffset;
    Boolean             _bStylusPressure;
    WPCurrentStrokeView * _currentStrokeView;
    WPInkView *         _inkView;
    Boolean             _bZeroPressure;
    int                 _lastPressure;
    BOOL                _iPenDown;
    int                 _nAdded;
    BOOL                _bPalmRest;
    BOOL                _isObserver;
    NSTimer *          _timerRecognizer;
}

@property(nonatomic, readwrite) float			strokeWidth;
@property(nonatomic, readonly)  UIColor *		strokeColor;
@property(nonatomic, readonly)  Boolean			asyncRecoEnabled;
@property(nonatomic, readonly)  Boolean			asyncInkCollector;
@property(nonatomic, weak)    WritePadInputView *	inputPanel;
@property(nonatomic, readonly)  INK_DATA_PTR		inkData;

@property(nonatomic, assign)    int					strokeLen;
@property(nonatomic, assign)    CGStroke			ptStroke;

- (void) reloadOptions;
- (void) empty;
- (BOOL) deleteLastStroke;
- (void) enableGestures:(GESTURE_TYPE)gestures whenEmpty:(BOOL)bEmpty;
- (BOOL) enableAsyncRecognizer:(BOOL)bEnable;
- (BOOL) enableAsyncInk:(BOOL)bEnable;
- (BOOL) startAsyncRecoThread;
- (void) stopAsyncRecoThread;
- (NSUInteger) strokeCount;
- (BOOL) processShortcut:(NSString *)name;
- (void) shortcutsEnable:(BOOL)bEnable delegate:(id)del uiDelegate:(id)uiDel;

@property(assign) id<WritePadInputPanelDelegate> delegate;

@end

@protocol WritePadInputPanelDelegate<NSObject>
@optional

- (void) WritePadInputPanelResultReady:(WritePadInputPanel*)inkView theResult:(NSString*)string;
- (void) WritePadInputPanelAsyncResultReady:(WritePadInputPanel*)inkView theResult:(NSString*)string;
- (BOOL) WritePadInputPanelRecognizedGesture:(WritePadInputPanel*)inkView withGesture:(GESTURE_TYPE)gesture isEmpty:(BOOL)bEmpty;
- (void) WritePadInputPanelChangeLanguage:(WritePadInputPanel*)inkView;

@end
