//  JJ_Char_Detector
//
//  Created by Japahar Jose on 24/08/19.
//  Copyright © 2019 *compasquare*. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WritePadInputView.h"

@interface AsyncResultView : UIView
{
@private
	NSString *	text;
	UIFont *	font;
	NSMutableArray *	words;
	NSInteger	selectedWord;
	Boolean		resultError;
	UIFont *	fontError;
	CGRect		_rCurrWord;
}

@property(nonatomic, strong ) NSMutableArray *	words;
@property(nonatomic, weak)    WritePadInputView *	inputPanel;
@property(nonatomic, strong ) NSString *	text;

- (void) empty;

- (NSInteger) learnNewWords;

@end
