//  JJ_Char_Detector
//
//  Created by Japahar Jose on 24/08/19.
//  Copyright © 2019 *compasquare*. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Shortcuts.h"

@class WritePadInputPanel;
@class AsyncResultView;

@protocol WritePadInputViewDelegate;

#define SHROTHAND_BUTTON_TAG    106

@interface UIResultsView : UIView 

@end

@interface WritePadInputView : UIView 
{
	WritePadInputPanel *	inkCollector;
	AsyncResultView *		resultView;
    UIResultsView *         uiResultView;
	NSString *				placeholder;
    
@private
	CGGradientRef			myGradient;
	Boolean					bHasInk;
	NSTimer *				holdTimer;
	Boolean					_bIgnoreActionKey;
	NSMutableArray *		_buttons;
	CGFloat                 markerPosition;
	Boolean					_markerSelected;
    BOOL                    _showCmdButton;

}

@property (nonatomic, retain, readonly) WritePadInputPanel *	inkCollector;
@property (nonatomic, retain, readonly) AsyncResultView *		resultView;
@property (nonatomic, retain) NSString *	placeholder;
@property (nonatomic, assign) id			delegate;
@property (nonatomic, assign) Boolean		showMarker;
@property (nonatomic, retain) UIButton *    cmdButton;
@property (nonatomic, retain) UIButton *    penButton;

@property (nonatomic, assign) BOOL drawGrid;

+ (WritePadInputView *) sharedInputPanel;
+ (void) destroySharedInputPanel;

+ (UIColor *) colorForElement:(NSString *)element;
+ (BOOL) flagForElement:(NSString *)element;
+ (void) initColorsWithStyle:(NSInteger)style;
+ (CGFloat) floatForElement:(NSString *)element;

- (void) empty;
- (void) setHasInk:(Boolean)bHasInk;
- (void) setMarkerPosition:(CGFloat)pos;
- (CGRect) getMarkerRect;
- (void) moveMarkerToLocation:(CGPoint)location  selected:(BOOL)sel;
- (CGFloat) getMarkerPosition;
- (void) showCommandButton:(BOOL)show withCommand:(NSString *)command;

@end


@protocol WritePadInputViewDelegate<NSObject>
@optional

- (void) writePadInputPanelSpeakEvent:(WritePadInputView*)panel didStart:(BOOL)start;
- (void) writePadInputKeyPressed:(WritePadInputView*)panel keyText:(NSString*)string withSender:(id)sender;
- (UIView *) writePadInputPanelPositionAltPopover:(CGRect *)pRect;

@end
