//  JJ_Char_Detector
//
//  Created by Japahar Jose on 24/08/19.
//  Copyright © 2019 *compasquare*. All rights reserved.
//


#ifndef __LISTATE_H
#define __LISTATE_H

typedef enum __E_LIG_STATE {
	LIG_STATE_UNDEF  = 0,
	LIG_STATE_OFTEN  = 1,
	LIG_STATE_RARELY = 2,
	LIG_STATE_NEVER  = 3
}E_LIG_STATE;


#define LIG_FIRST_LETTER 0x20
#define LIG_LAST_LETTER  0xFF
#define LIG_NUM_LETTERS  (LIG_LAST_LETTER - LIG_FIRST_LETTER + 1)
#if LIG_NUM_LETTERS <= 0
	#error
#endif
#define LIG_LET_NUM_GROUPS     8
#define LIG_NUM_BITS_PER_GROUP 2
#define LIG_NUM_BIT_GROUP_MASK 0x3

#define LIG_STATES_SIZE \
	(LIG_NUM_LETTERS * LIG_LET_NUM_GROUPS * LIG_NUM_BITS_PER_GROUP / 8)

typedef unsigned char LIGStatesType[LIG_STATES_SIZE];


/*
 * Sets state for a given letter and group.
 * Returns 0 if letter and group are in the allowed range, -1 otherwise.
 */
int  LIGSetGroupState(LIGStatesType *ioGStates,
                             int           inLetter,
					         int           inGroup,
                             E_LIG_STATE   inGroupState);

/*
 * Returns state for a given letter and group.
 */
E_LIG_STATE LIGGetGroupState(const LIGStatesType *inGStates,
                             int                 inLetter,
					         int                 inGroup);

#endif /* __LISTATE_H */
