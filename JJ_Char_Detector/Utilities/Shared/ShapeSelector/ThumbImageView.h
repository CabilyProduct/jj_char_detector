//  JJ_Char_Detector
//
//  Created by Japahar Jose on 24/08/19.
//  Copyright © 2019 *compasquare*. All rights reserved.
//


#import <UIKit/UIKit.h>

@protocol ThumbImageViewDelegate;

#define THUMB_HEIGHT			102
#define THUMB_V_PADDING			10
#define THUMB_H_PADDING			10
#define AUTOSCROLL_THRESHOLD	30
#define MAX_FOLDER				15


@interface ThumbImageView : UIView
{
    NSString *	letter;
    
    /* ThumbImageViews have a "home," which is their location in the containing scroll view. Keeping this distinct */
    /* from their frame makes it easier to handle dragging and reordering them. We can change their relative       */
    /* positions by changing their homes, without having to worry about whether they have currently been dragged   */
    /* somewhere else. Also, we don't lose track of where they belong while they are being moved.                  */
    CGRect home;
	NSInteger index;
	BOOL	selected;
    
    BOOL	dragging;
    CGPoint touchLocation; // Location of touch in own coordinates (stays constant during dragging).
}

@property (nonatomic, weak) id <ThumbImageViewDelegate> delegate;
@property (nonatomic, assign) CGRect		home;
@property (nonatomic, assign) NSInteger		index;
@property (nonatomic, assign) CGPoint		touchLocation;


- (id) initWithLetter:(NSString *)str;
- (void) goHome;  // animates return to home location
- (void) moveByOffset:(CGPoint)offset; // change frame lo
- (void) setSelected:(BOOL)s;

@end



@protocol ThumbImageViewDelegate <NSObject>

@optional
- (void)thumbImageViewWasTapped:(ThumbImageView *)tiv;
- (void)thumbImageViewStartedTracking:(ThumbImageView *)tiv;
- (void)thumbImageViewMoved:(ThumbImageView *)tiv;
- (void)thumbImageViewStoppedTracking:(ThumbImageView *)tiv;

@end

