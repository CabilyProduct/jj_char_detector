//  JJ_Char_Detector
//
//  Created by Japahar Jose on 24/08/19.
//  Copyright © 2019 *compasquare*. All rights reserved.
//



#ifndef __LIDATADF_H
#define __LIDATADF_H

#include "letimg.h"

const LIDBType* LIGetLetterDB( int languageID );

#endif // __LIDATADF_H
