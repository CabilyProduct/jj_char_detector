

// #include "RecoTypes.h"
#include "ShapeAPI.h"

static const unsigned char let_img_img_eng[] = {

#include "lidt_eng.h"

};

static const unsigned char let_img_img_ger[] = {

#include "lidt_ger.h"

};

static const unsigned char let_img_img_frn[] = {

#include "lidt_fre.h"

};

const LIDBType *LIGetLetterDB( int languageID )
{
    switch( languageID )
    {
        case LANGUAGE_FRENCH :
        case LANGUAGE_ITALIAN :
        case LANGUAGE_PORTUGUESE :
        // case LANGUAGE_PORTUGUESEB :
            return (LIDBType*)let_img_img_frn;

        case LANGUAGE_GERMAN :
            return (LIDBType*)let_img_img_ger;
    }
	return (LIDBType*)let_img_img_eng;
}
