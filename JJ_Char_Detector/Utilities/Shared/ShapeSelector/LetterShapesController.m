//  JJ_Char_Detector
//
//  Created by Japahar Jose on 24/08/19.
//  Copyright © 2019 *compasquare*. All rights reserved.
//

#import "LetterShapesController.h"
#import "RecognizerManager.h"
#import "UIConst.h"

#define LETTERS_STRIP_HEIGHT		86

@implementation LetterShapesController

 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/

- (void)createThumbScrollView:(CGRect)rect
{
	thumbScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake( 0, 0, rect.size.width, rect.size.height)];
	thumbScrollView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
	[thumbScrollView setCanCancelContentTouches:NO];
	[thumbScrollView setClipsToBounds:NO];
	
	// now place all the thumb views as subviews of the scroll view 
	// and in the course of doing so calculate the content width
	CGFloat xPosition = THUMB_H_PADDING;
	CGFloat xOffset = 0;
	NSString * chars = [LetterGroupView getCharSet];
	for ( NSInteger i = 0; i < [chars length]; i++ ) 
	{
		unichar ch = [chars characterAtIndex:i];
		NSString * strName = [NSString stringWithCharacters:&ch  length:1];
		if ( nil != strName ) 
		{
			ThumbImageView *thumbView = [[ThumbImageView alloc] initWithLetter:strName];
			[thumbView setDelegate:self];
			thumbView.userInteractionEnabled = YES;
			thumbView.index = i;
			CGRect frame = [thumbView frame];
			frame.origin.y = THUMB_V_PADDING;
			frame.origin.x = xPosition;
			frame.size.width = THUMB_HEIGHT - 2 * THUMB_V_PADDING;
			frame.size.height = THUMB_HEIGHT - 2 * THUMB_V_PADDING;
			[thumbView setFrame:frame];
			[thumbView setHome:frame];
			[thumbView setBackgroundColor:[UIColor clearColor]];
			[thumbScrollView addSubview:thumbView];
			xPosition += (frame.size.width + THUMB_H_PADDING);
			
			if ( i == selectedLetter )
			{
				[thumbView setSelected:YES];
				if ( xPosition > rect.size.width )
				{
					// scroll to the position
					xOffset = ((xPosition - rect.size.width) + THUMB_H_PADDING);
				}
			}
		}
	}
	[thumbScrollView setContentSize:CGSizeMake(xPosition, rect.size.height)];
	[thumbScrollView setContentOffset:CGPointMake( xOffset, 0 )];
}    

- (void)createSlideUpView:(CGRect)rect
{    
	[self createThumbScrollView:rect];
	
	//float thumbHeight = [thumbScrollView frame].size.height;
	
	// create container view that will hold scroll view and label
	//CGRect frame = CGRectMake(CGRectGetMinX(bounds), CGRectGetMaxY(bounds), bounds.size.width, thumbHeight);
	slideUpView = [[UIView alloc] initWithFrame:rect];
	slideUpView.autoresizesSubviews = YES;
	slideUpView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin);
	slideUpView.backgroundColor = [UIColor colorWithRed:217.0/255.0 green:220.0/255.0 blue:225.0/255.0 alpha:1.0];
	
	// [slideUpView setOpaque:NO];
	// [slideUpView setAlpha:1.0];
	
	[slideUpView addSubview:thumbScrollView];
	// add subviews to container view
	[[self view] addSubview:slideUpView];
}    


// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView 
{
	[super loadView];
	
	selectedLetter = 0;
	
	CGRect rect = self.view.bounds;
	
    CGRect r = self.navigationController.navigationBar.frame;
    rect.origin.y = r.size.height + r.origin.y;
    //  rect.origin.y += kToolbarHeight;

    rect.size.height -= (THUMB_HEIGHT);
    
    letterView = [[LetterGroupView alloc] initWithFrame:rect recognizer:[[RecognizerManager sharedManager] recognizer]];
	letterView.autoresizesSubviews = YES;
	letterView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
	// letterView.backgroundColor = [UIColor clearColor];	// use the table view background color
	[letterView selectLetter:selectedLetter];
	[self.view addSubview:letterView];
	
	self.navigationItem.title = NSLocalizedString( @"Letter Shapes", @"" );
	
	rect.origin.y = rect.size.height;
	rect.size.height = THUMB_HEIGHT;
	
	[self createSlideUpView:rect];
}

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}
*/

- (void) viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
	
	[letterView saveShapes];
}


- (void)didReceiveMemoryWarning 
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

#pragma mark ThumbImageViewDelegate methods

- (void)thumbImageViewWasTapped:(ThumbImageView *)tiv 
{
	if ( tiv.index != selectedLetter )
	{
		ThumbImageView * selView = [[thumbScrollView subviews] objectAtIndex:selectedLetter];
		if ( selView != nil )
		{
			[selView setSelected:NO];
		}
		selectedLetter = tiv.index;
		[letterView selectLetter:selectedLetter];
		[tiv setSelected:YES];
	}
}

- (void)thumbImageViewStartedTracking:(ThumbImageView *)tiv 
{
    // [thumbScrollView bringSubviewToFront:tiv];
}

- (void)thumbImageViewMoved:(ThumbImageView *)draggingThumb
{
}

- (void)thumbImageViewStoppedTracking:(ThumbImageView *)tiv 
{
}


@end
