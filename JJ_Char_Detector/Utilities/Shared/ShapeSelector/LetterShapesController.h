//  JJ_Char_Detector
//
//  Created by Japahar Jose on 24/08/19.
//  Copyright © 2019 *compasquare*. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LetterGroupView.h"
#import "ThumbImageView.h"

@interface LetterShapesController : UIViewController <ThumbImageViewDelegate>
{
@private
	LetterGroupView *	letterView;
	UIScrollView *	thumbScrollView;
	UIView       *	slideUpView; // Contains thumbScrollView and a label giving credit for the images.
	NSInteger		selectedLetter;

}

@end
