//  JJ_Char_Detector
//
//  Created by Japahar Jose on 24/08/19.
//  Copyright © 2019 *compasquare*. All rights reserved.
//


#ifndef LIDATA_H_INC
#define LIDATA_H_INC

#if  defined( FOR_GERMAN )
#include  "lidt_ger.h"
#elif  defined( FOR_FRENCH )
#include  "lidt_fre.h"
#else /* assumed FOR_ENGLISH or FOR_INTERNATIONAL */
#include  "lidt_eng.h"
#endif /* FOR_languages */ 

#endif // LIDATA_H_INC
