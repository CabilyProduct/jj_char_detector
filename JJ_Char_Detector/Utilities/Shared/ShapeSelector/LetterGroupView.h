//  JJ_Char_Detector
//
//  Created by Japahar Jose on 24/08/19.
//  Copyright © 2019 *compasquare*. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShapeAPI.h"
#import "RecognizerWrapper.h"

@interface LetterGroupView : UIView 
{
@private
	const LIDBType *	_lidb;
	LIGStatesType		_groupstates;
	UCHR				_selchar;
	RECOGNIZER_PTR		_recognizer;
	UIColor  *			_color;
	NSInteger			_width;
	LILetImgDrawType	_lidraw;
	BOOL				needRecalcLayout;
}

- (id)initWithFrame:(CGRect)frame recognizer:(RECOGNIZER_PTR)recognizer;
+ (NSString *) getCharSet;
- (void) selectLetter:(NSInteger)index;
- (void) saveShapes;
- (void) setNeedsRecalcLayout;

@end
