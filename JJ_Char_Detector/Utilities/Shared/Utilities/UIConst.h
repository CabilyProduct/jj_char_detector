//  JJ_Char_Detector
//
//  Created by Japahar Jose on 24/08/19.
//  Copyright © 2019 *compasquare*. All rights reserved.
//

#pragma once

#define kInsertValue				8.0
#define kUIRowHeight				50.0
#define kUIRowLabelHeight			22.0
#define kFolderRowHeight			72.0
#define kSwitchButtonWidth			98.0
#define kCellLeftOffset				8.0
#define kCellTopOffset				12.0
#define kTextFieldHeight			31.0
#define kToolbarHeight				44.0
#define kNewWordCellHeight			56.0
#define kWordCellHeight				44.0
#define kTextFieldWidth				260.0	// initial width, but the table cell will dictact the actual width
#define kSwitchButtonHeight			30.0

#ifdef _DEVICE_IPAD_
#define DEFAULT_BACKGESTURELEN		300
#define MIN_BACKGESTURELEN			200
#define MAX_BACKGESTURELEN			500
#else
#define DEFAULT_BACKGESTURELEN		200
#define MIN_BACKGESTURELEN			100
#define MAX_BACKGESTURELEN			300
#endif // _DEVICE_IPAD_
#define DEFAULT_PENWIDTH			10.0
#define DEFAULT_RECODELAY			1.5
#define MIN_DELAY					0.2

#define IS_PHONE                    (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)

#define kInputPanelHeight			(IS_PHONE ? 190.0 : 280.0)
#define kBottomOffset				22

#define DEFAULT_STROKE_LEN          1000
#define DEFAULT_TOUCHANDHOLDDELAY	0.6
#define kGridStep					85.0

