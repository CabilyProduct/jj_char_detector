//  JJ_Char_Detector
//
//  Created by Japahar Jose on 24/08/19.
//  Copyright © 2019 *compasquare*. All rights reserved.
//

#import "RectObject.h"


/////////////////////////////////////////////////////////////
// RectObject

@implementation RectObject

@synthesize	rect;
@synthesize	flags;

- (id)initWithRect:(CGRect)inRect
{
    self = [super init];
	if ( self )
	{
		rect = inRect;
		flags = 0;
	}
	return self;
}

- (id)initWithRect:(CGRect)inRect andFlags:(NSUInteger)f
{
    self = [super init];
	if ( self )
	{
		rect = inRect;
		flags = f;
	}
	return self;
}


@end

