//  JJ_Char_Detector
//
//  Created by Japahar Jose on 24/08/19.
//  Copyright © 2019 *compasquare*. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "WritePadInputView.h"
#import "WritePadInputPanel.h"
#import "Shortcuts.h"
#import "InkCollectorView.h"
#import "SuggestionsView.h"

typedef enum {
    InputSystem_Default = -1,
    InputSystem_InputPanel = 0,
    InputSystem_WriteAnywhere,
    InputSystem_Keyboard
} InputSystem;

@interface WPTextView : UITextView <WritePadInputPanelDelegate, WritePadInputViewDelegate, ShortcutsDelegate, InkCollectorViewDelegate, SuggestionsViewDelegate>
{
@private
    InkCollectorView * inkCollector;
}

+ (WPTextView *) createTextView:(CGRect)frame;

- (void) setInputMethod:(InputSystem)inputSystem;
- (void) processTouchAndHoldAtLocation:(CGPoint)location;
- (void) tapAtLocation:(NSSet *)touches withEvent:(UIEvent *)event;
- (void) selectTextFromPosition:(CGPoint)from toPosition:(CGPoint)to;
- (void) hideSuggestions;
- (void) reloadOptions;
- (void) scrollToVisible;

@property (nonatomic, retain) InkCollectorView * inkCollector;
@property (nonatomic, readonly ) InputSystem  inputSystem;


@end
