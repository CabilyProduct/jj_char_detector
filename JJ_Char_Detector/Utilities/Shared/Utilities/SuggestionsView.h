//  JJ_Char_Detector
//
//  Created by Japahar Jose on 24/08/19.
//  Copyright © 2019 *compasquare*. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "RecognizerApi.h"
#import "RecognizerWrapper.h"

@protocol SuggestionsViewDelegate;

@interface SuggestionsView : UIScrollView <UIScrollViewDelegate>
{
@private
	NSMutableArray *	_words;
	NSInteger			selectedWord;
	NSInteger			textLength;
    NSMutableArray *    buttons;
	NSTimer *           hideTimer;
    
    UIFont *            _italic;
    UIFont  *           _font;
    NSLock *            _lock;
}

+ (SuggestionsView *) sharedSuggestionsView;

- (BOOL) showResultsInRect:(CGRect)rPosition inFrame:(CGRect)inFrame;
- (void) updateWordList:(NSString *)text inFrame:(CGRect)inFrame  posiiton:(CGFloat)position spellCheck:(BOOL)spell;
- (BOOL) showResultsinKeyboard:(UIView *)keyboard inRect:(CGRect)rect;
- (void) setBarHeight:(CGFloat)height;

+ (CGFloat)getHeight;

@property(assign) id<SuggestionsViewDelegate>	suggestions_delegate;

@property (nonatomic, retain) UIColor * colorFirstResult;
@property (nonatomic, retain) UIColor * colorAlertResult;
@property (nonatomic, retain) UIColor * colorOtherResult;
@property (nonatomic, retain) UIColor * colorBackground;

@end

@protocol SuggestionsViewDelegate <NSObject>
@optional

- (void) suggestionsWordSelected:(SuggestionsView*)caller theResult:(NSString *)word spellWord:(NSString *)spellWord;
- (void) suggestionsWillDissapear:(SuggestionsView*)caller;
- (BOOL) suggestionsViewShouldTimeout:(SuggestionsView*)caller;

@end
