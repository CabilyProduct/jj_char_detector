//  JJ_Char_Detector
//
//  Created by Japahar Jose on 24/08/19.
//  Copyright © 2019 *compasquare*. All rights reserved.
//

#pragma once

#import <UIKit/UIKit.h>

#define CFSafeRelease( x )  if (NULL!=x) {CFRelease(x); x = NULL;}
#define NSStringReplace(str,find,replace) [str replaceOccurrencesOfString:find withString:replace options:NSLiteralSearch range:NSMakeRange(0,[str length])]

#ifdef __IPHONE_4_0
#define	SET_DELEGATE(del)	del
#else
#define SET_DELEGATE(del)
#endif // __IPHONE_4_0

enum
{
    UIDeviceResolution_Unknown          = 0,
    UIDeviceResolution_iPhoneStandard   = 1,    // iPhone 1,3,3GS Standard Display  (320x480px)
    UIDeviceResolution_iPhoneRetina35   = 2,    // iPhone 4,4S Retina Display 3.5"  (640x960px)
    UIDeviceResolution_iPhoneRetina4    = 3,    // iPhone 5 Retina Display 4"       (640x1136px)
    UIDeviceResolution_iPadStandard     = 4,    // iPad 1,2 Standard Display        (1024x768px)
    UIDeviceResolution_iPadRetina       = 5     // iPad 3 Retina Display            (2048x1536px)
};

typedef NSUInteger UIDeviceResolution;

#define SET_CURR_POPOVER(pop)			[utils setCurrPopover:(pop)];
#define HIDE_CURR_POPOVER				[utils dismissCurrentPopover];

#define IS_PHONE                        (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)

#define NSLocalizedTableTitle( str )    [NSString stringWithFormat:@"   %@", NSLocalizedString( str, @"" )]
#define LOC( str )                      NSLocalizedString( str, @"" )
#define LOCT( str )                     NSLocalizedTableTitle( str )

/////////////////////////////////////////////////////////////////////////////

@interface utils : NSObject
{

}

+ (NSString *) appNameAndVersionNumberDisplayString;
+ (NSString *) shortFileName:(NSString *)fileName;
+ (Boolean) isPhone;
+ (UInt32) _uiColorToColorRef:(UIColor *)color;
+ (UIColor *) _uiColorRefToColor:(UInt32)rgb;
+ (CGFloat) _uiColorAlpha:(UIColor *)color;
+ (CGFloat) calcWidth:(CGFloat)weight pressure:(int)pressure;
+ (NSInteger) getMajorOSVersion;
+ (UIDeviceResolution)resolution;

+ (NSURL *) sharedRecoDataURL;
+ (void) useLocalRecoDefaults:(BOOL)use;
+ (NSUserDefaults *) recoGroupUserDefaults;

+ (BOOL) isRetina;

+ (CGFloat) distanceFrom:(CGPoint)from toPoint:(CGPoint)to;

+ (UIImage *) colorImageWithImage:(UIImage *)img color:(UIColor *)col mode:(CGBlendMode)m;
+ (UIImage *) colorImageWithName:(NSString *)name color:(UIColor *)col mode:(CGBlendMode)m;

@end

