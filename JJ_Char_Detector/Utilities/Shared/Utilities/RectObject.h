//  JJ_Char_Detector
//
//  Created by Japahar Jose on 24/08/19.
//  Copyright © 2019 *compasquare*. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RectObject : NSObject
{
	CGRect		rect;
	NSUInteger	flags;
}

@property (nonatomic, assign, readonly) CGRect rect;
@property (nonatomic, assign) NSUInteger	flags;

- (id)initWithRect:(CGRect)inRect;
- (id)initWithRect:(CGRect)inRect andFlags:(NSUInteger)flags;

@end

