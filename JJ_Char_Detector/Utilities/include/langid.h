//  JJ_Char_Detector
//
//  Created by Japahar Jose on 24/08/19.
//  Copyright © 2019 *compasquare*. All rights reserved.
//


#ifndef LANGID_H
#define LANGID_H

#define LANGUAGE_NONE			0
#define LANGUAGE_ENGLISH		1
#define LANGUAGE_FRENCH			2
#define LANGUAGE_GERMAN			3
#define LANGUAGE_SPANISH		4
#define LANGUAGE_ITALIAN		5
#define LANGUAGE_SWEDISH		6
#define LANGUAGE_NORWEGIAN		7
#define LANGUAGE_DUTCH		    8
#define LANGUAGE_DANISH		    9
#define LANGUAGE_PORTUGUESE	    10
#define LANGUAGE_PORTUGUESEB    11
#define LANGUAGE_MEDICAL        12
#define LANGUAGE_FINNISH		13
#define LANGUAGE_INDONESIAN		14
#define LANGUAGE_ENGLISHUK		15
#define LANGUAGE_SIZE           16

#define LANG_MEDICAL            0x71        // special entry for medical dictionary

#endif // LANGID_H
